from django.conf import settings
from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _
from googleapiclient.discovery import build


def home(request):
    template = loader.get_template("django_company_cms_search/home.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Website")
    template_opts["content_title_sub"] = _("Search")

    query = request.GET.get("q", None)

    if query is None:
        return HttpResponse(template.render(template_opts, request))

    service = build(
        "customsearch", "v1", developerKey=settings.GOOGLE_CSE_RESTRICTED_API_KEY
    )

    search_results = (
        service.cse()
        .siterestrict()
        .list(
            q=query,
            cx=settings.GOOGLE_CSE_RESTRICTED_CX_ID,
        )
        .execute()
    )

    if "items" in search_results:
        template_opts["search_results"] = search_results["items"]

    return HttpResponse(template.render(template_opts, request))
